import React from 'react';
import { GoogleMap, Marker, withGoogleMap } from "react-google-maps"
import { STATUS } from './App';
const MapMarker = withGoogleMap(({ isMarkerShown, lat, lng }) =>
    <GoogleMap
        defaultZoom={12}
        defaultCenter={{ lat, lng }}
    >
        {isMarkerShown && <Marker position={{ lat, lng}} />}
    </GoogleMap>
);
export const SearchResult = ({ status, data, error }) => {
    const { query, location, hotel } = data || {};
    
    switch(status) {
        case STATUS.LOADING: return (<h1>Loading....</h1>);
        case STATUS.ERRORED: return (<h1>Could not data: {error}</h1>);
        case STATUS.LOADED: return (<div style={{ textAlign: 'left', width: '80%', margin: 'auto' }}>
            {
                (hotel.status === 'OK')
                ? (<div>
                    <h4>Outlet Name: {hotel.data.properties.name}</h4>
                    <div style={{ position: 'relative', width: '100%' }}>
                    <MapMarker
                        containerElement={<div style={{ height: `300px` }} />}
                        mapElement={<div style={{ height: `300px` }} />}
                        lat={hotel.data.geometry.coordinates[0][0][1]}
                        lng={hotel.data.geometry.coordinates[0][0][0]}
                    isMarkerShown />
                    </div>
                </div>)
                : (<h1>No outlet found</h1>)
            }
            <h4>Query: {query.location}</h4>
            <h5>Location: {location.title}</h5>
            <h5>City: {location.address.city}</h5>
            <h5>Country: {location.address.countryName}</h5>
        </div>)
        default: return (<h1>Search to get Nearby Hotel</h1>);
    }
};

