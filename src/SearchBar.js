import React, { useState, useEffect } from 'react';
import './SearchBar.css';
const SearchBar = ({ onSubmit }) => {
    const [ searchStr, setSearchStr ] = useState('');
    const submitLocation = () => {
        if(searchStr.trim().length > 0 && onSubmit) {
            onSubmit(searchStr);
        }
    }
    return <div>
        <input
    type="text"
    onChange={e => { setSearchStr(e.target.value) }}
    value={searchStr} placeholder="Search Location..."
    onKeyDown={e => { if(e.keyCode === 13) { submitLocation(); } }} />
    <button onClick={submitLocation}>Search</button>
    </div>
}
export default SearchBar;