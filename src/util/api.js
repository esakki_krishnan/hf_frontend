const API_URL='http://localhost:8000'
export function getHotelLocation(searchString) {
    console.log('getHotelLocation', searchString)
    return fetch(`${API_URL}/hotels?location=${searchString}`)
    .then(d => { console.log('d', d); return d.json(); })
    .catch(e => { console.log('e', e); return e;})
}