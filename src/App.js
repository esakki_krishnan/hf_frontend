import React, { useState } from 'react';
import './App.css';
import SearchBar from './SearchBar';
import { getHotelLocation } from './util/api';
import { SearchResult } from './SearchResult';

export const STATUS = {
  NOT_LOADED: 'NOT_LOADED',
  LOADING: 'LOADING',
  LOADED: 'LOADED',
  ERRORED: 'ERRORED',
}

function App() {
  const [ { status, data, error }, setQueryStatus ] = useState({ status: STATUS.NOT_LOADED });
  const onSearch = (searchStr) => {
    console.log('searchStr', searchStr);
    setQueryStatus({ status: STATUS.LOADING });
    getHotelLocation(searchStr)
            .then(d => {
              console.log('d', d);
              setQueryStatus({ status: STATUS.LOADED, data: d });
            })
            .catch(e => {
              console.error(e);
              setQueryStatus({ status: STATUS.ERRORED, error: e });
            })
  };
  return (
    <div className="App">
      <SearchBar onSubmit={onSearch} />
      <SearchResult status={status} error={error} data={data} />
    </div>
  );
}

export default App;
